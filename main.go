package main

import (
	"app/converter"
	"app/parser"
	"bytes"
	"encoding/csv"
	"errors"
	"flag"
	"io"
	"log"
	"os"
	"path"
	"strings"
)

var (
	inputPath = flag.String("input", `D:\World_of_Tanks_RU\replays`, "Входной файл или путь")
	debugMode = flag.Bool("d", false, "Режим отладки (один файл)")
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.Parse()

	if *debugMode {
		debugFile(*inputPath)
	} else {
		convertToCSV(*inputPath)
	}

}

func debugFile(filename string) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatalln(err)
	}

	raw, err := io.ReadAll(file)
	if err != nil {
		log.Fatalln(err)
	}

	err = file.Close()
	if err != nil {
		log.Fatalln(err)
	}

	info, data, err := parser.ParseRaw(raw)
	if err != nil {
		log.Fatalln(err)
	}

	log.Println(string(info))
	log.Println(string(data))

	info1, data1, err := parser.Parse(raw)
	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("%#+v\n", info1)
	log.Printf("%#+v\n", data1)

}

func convertToCSV(pathToDir string) {
	file, err := os.Open(pathToDir)
	if err != nil {
		log.Fatalln(err)
	}

	names, err := file.Readdirnames(0)
	if err != nil {
		log.Fatalln(err)
	}

	file.Close()

	out, err := os.Create("out.csv")
	if err != nil {
		log.Fatalln(err)
	}
	defer out.Close()

	w := csv.NewWriter(out)

	err = w.Write(converter.CSVHeader1())
	if err != nil {
		log.Fatalln(err)
	}
	w.Flush()

	total := len(names)

	for index, name := range names {
		log.Printf("%d/%d", index+1, total)

		if !strings.Contains(name, ".wotreplay") {
			continue
		}

		data, err := parseFile(path.Join(pathToDir, name))
		if err != nil {
			if errors.Is(err, parser.BrokenDataError) {
				log.Println(name, err)
			} else {
				log.Fatalln(name, err)
			}

			continue
		}

		err = w.Write(data)
		if err != nil {
			log.Fatalln(err)
		}
		w.Flush()
	}
}

func parseFile(filename string) ([]string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}

	buff := &bytes.Buffer{}
	_, _ = buff.ReadFrom(file)
	file.Close()

	battleInfo, fullData, err := parser.Parse(buff.Bytes())
	if err != nil {
		return nil, err
	}

	return converter.ToCSV1(battleInfo, fullData), nil
}
