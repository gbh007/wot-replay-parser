package converter

import (
	"app/model"
	"fmt"
)

func CSVHeader1() []string {
	return []string{
		"Танк",
		"Название сервера",
		"Название карты на русском",
		"Время боя",
		"Код карты",
		"Тип боя",
		"Имя игрока",
		"Время жизни в секундах",

		"Итого Ассист",
		"Урона нанесено",
		"Фраги",

		"Начислено кредитов",
		"Чистая прибыль",
		"Стоимость патронов",
		"Стоимость ремонта и расходников",

		"Начислено опыта без према и бонусов",
		"Итого опыта",
	}
}

func ToCSV1(battleInfo *model.BattleInfo, fullData *model.FullData) []string {
	return []string{
		battleInfo.PlayerVehicle,
		battleInfo.ServerName,
		battleInfo.MapDisplayName,
		battleInfo.DateTime,
		battleInfo.MapName,
		fmt.Sprintf("%d", battleInfo.BattleType),
		battleInfo.PlayerName,
		fmt.Sprintf("%d", fullData.LifeTime),

		fmt.Sprintf("%d", fullData.AssistSum()),
		fmt.Sprintf("%d", fullData.DamageDealt),
		fmt.Sprintf("%d", fullData.Kills),

		fmt.Sprintf("%d", fullData.OriginalCredits),
		fmt.Sprintf("%d", fullData.TotalFarm()),
		fmt.Sprintf("%d", fullData.TotalLoadCost()),
		fmt.Sprintf("%d", fullData.TotalRepairAndEquip()),

		fmt.Sprintf("%d", fullData.OriginalXP),
		fmt.Sprintf("%d", fullData.Xp),
	}
}
