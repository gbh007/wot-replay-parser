package model

type BattleInfo struct {
	// Танк
	PlayerVehicle string `json:"playerVehicle,omitempty"`
	// Версия
	ClientVersionFromExe string `json:"clientVersionFromExe,omitempty"`
	// Регион
	RegionCode string `json:"regionCode,omitempty"`
	// ИД игрока
	PlayerID int64 `json:"playerID,omitempty"`
	// Название сервера
	ServerName string `json:"serverName,omitempty"`
	// Название карты на русском
	MapDisplayName string `json:"mapDisplayName,omitempty"`
	// Время боя
	DateTime string `json:"dateTime,omitempty"`
	// Код карты
	MapName string `json:"mapName,omitempty"`
	// Тип боя
	BattleType int64 `json:"battleType,omitempty"`
	// Имя игрока
	PlayerName string `json:"playerName,omitempty"`
}
