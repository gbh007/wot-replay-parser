package model

type BattleData_Farm struct {
	// Золото ???
	Gold int64 `json:"gold,omitempty"`
	// Кредитов поступило в резервное хранилище
	PiggyBank int64 `json:"piggyBank,omitempty"`
	// Кредиты с личных резервов
	BoosterCredits int64 `json:"boosterCredits,omitempty"`
	// Золото ???
	OriginalGold int64 `json:"originalGold,omitempty"`
	// Всего кредитов (грязные)
	Credits int64 `json:"credits,omitempty"`
	// Начислено кредитов за бой (без бустеров и бонусов)
	OriginalCredits int64 `json:"originalCredits,omitempty"`
	// Кредиты за взвод
	PremSquadCredits int64 `json:"premSquadCredits,omitempty"`
	// Золото за событие ???
	EventGold int64 `json:"eventGold,omitempty"`
	// Итого за бой грязынми ???
	FactualCredits int64 `json:"factualCredits,omitempty"`
	// ???
	SubtotalCredits int64 `json:"subtotalCredits,omitempty"`
	// ???
	SubtotalGold int64 `json:"subtotalGold,omitempty"`
}

type BattleData_Cost struct {
	// Стоимость расходников
	AutoEquipCost []int64 `json:"autoEquipCost,omitempty"`
	// Ремонт (стоимость)
	Repair int64 `json:"repair,omitempty"`
	// ???
	AutoEquipBoostersCost []int64 `json:"autoEquipBoostersCost,omitempty"`
	// Стоимость ремонта
	AutoRepairCost int64 `json:"autoRepairCost,omitempty"`
	// Стоимость боекомплекта
	AutoLoadCost []int64 `json:"autoLoadCost,omitempty"`
}

func (c BattleData_Cost) Total() int64 {
	var sum int64

	for _, v := range c.AutoEquipCost {
		sum += v
	}

	for _, v := range c.AutoEquipBoostersCost {
		sum += v
	}

	for _, v := range c.AutoLoadCost {
		sum += v
	}

	sum += c.AutoRepairCost

	return sum
}

func (c BattleData_Cost) TotalRepairAndEquip() int64 {
	var sum int64

	for _, v := range c.AutoEquipCost {
		sum += v
	}

	sum += c.AutoRepairCost

	return sum
}

func (c BattleData_Cost) TotalLoadCost() int64 {
	var sum int64

	for _, v := range c.AutoLoadCost {
		sum += v
	}

	return sum
}
