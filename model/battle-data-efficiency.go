package model

type BattleData_Efficiency struct {
	BattleData_Efficiency_Assist
	// Знак класности
	MarkOfMastery int64 `json:"markOfMastery,omitempty"`
	// Прямые попадания
	DirectHits int64 `json:"directHits,omitempty"`
	// Продолжительность стана
	StunDuration float64 `json:"stunDuration,omitempty"`
	// Оглушено противников
	Stunned int64 `json:"stunned,omitempty"`
	// Фраги
	Kills int64 `json:"kills,omitempty"`
	// Процент урона от урона команды
	PercentFromTotalTeamDamage float64 `json:"percentFromTotalTeamDamage,omitempty"`
	// Выстрелы
	Shots int64 `json:"shots,omitempty"`
	// Количество станов
	StunNum int64 `json:"stunNum,omitempty"`
	// Обнаружено
	Spotted int64 `json:"spotted,omitempty"`
	// Прямые попадания по врагам
	DirectEnemyHits int64 `json:"directEnemyHits,omitempty"`
	// Урона нанесено
	DamageDealt int64 `json:"damageDealt,omitempty"`
	// Пробития
	PiercingEnemyHits int64 `json:"piercingEnemyHits,omitempty"`
	// Пробития
	Piercings int64 `json:"piercings,omitempty"`
}

type BattleData_Efficiency_Assist struct {
	// Засвет
	DamageAssistedRadio int64 `json:"damageAssistedRadio,omitempty"`
	// Асист станом
	DamageAssistedStun int64 `json:"damageAssistedStun,omitempty"`
	// Асист дымами
	DamageAssistedSmoke int64 `json:"damageAssistedSmoke,omitempty"`
	// Асист по гусле
	DamageAssistedTrack int64 `json:"damageAssistedTrack,omitempty"`
}

func (as BattleData_Efficiency_Assist) AssistSum() int64 {
	return as.DamageAssistedRadio + as.DamageAssistedSmoke + as.DamageAssistedStun + as.DamageAssistedTrack
}

type BattleData_Survive struct {
	// Осталось ХП
	Health int64 `json:"health,omitempty"`
	// Всего было ХП
	MaxHealth int64 `json:"maxHealth,omitempty"`
	// Потенциальный полученный урон
	PotentialDamageReceived int64 `json:"potentialDamageReceived,omitempty"`
	// Полученный урон
	DamageReceived int64 `json:"damageReceived,omitempty"`
	// Время жизни в секундах
	LifeTime int64 `json:"lifeTime,omitempty"`
	// Заблокировано броней
	DamageBlockedByArmor int64 `json:"damageBlockedByArmor,omitempty"`
	// Получено урона из кустов (не светящихся противников)
	DamageReceivedFromInvisibles int64 `json:"damageReceivedFromInvisibles,omitempty"`
}
