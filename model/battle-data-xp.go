package model

type BattleData_XP struct {
	// Итого опыта
	Xp int64 `json:"xp,omitempty"`
	// Итого чистого опыта
	FreeXP int64 `json:"freeXP,omitempty"`
	// Начислено опыта за бой (без учета премиума)
	OriginalXP int64 `json:"originalXP,omitempty"`
	// Начислено чистого опыта за бой (без учета премиума)
	OriginalFreeXP int64 `json:"originalFreeXP,omitempty"`
	// Начислено опыта за бой с учетом множителей (без учета бонусов)
	FactualXP int64 `json:"factualXP,omitempty"`
	// Чистое количество опыта за бой с учетом множителей (без учета бонусов)
	FactualFreeXP int64 `json:"factualFreeXP,omitempty"`
	// Количество полученого опыта (с учетом премиум аккаунта, без бонусов)
	SubtotalXP int64 `json:"subtotalXP,omitempty"`
	// Количество полученого чистого  опыта (с учетом премиум аккаунта, без бонусов)
	SubtotalFreeXP int64 `json:"subtotalFreeXP,omitempty"`
}
