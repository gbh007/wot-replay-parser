package model

type FullDataEnvelope struct {
	ArenaUniqueID int64                `json:"arenaUniqueID,omitempty"`
	Personal      map[string]*FullData `json:"personal,omitempty"`
}

type FullData struct {
	BattleData_Farm
	BattleData_Cost
	BattleData_XP
	BattleData_Efficiency
	BattleData_Survive
	// Есть ли прем
	IsPremium bool `json:"isPremium,omitempty"`
	// ИД ака
	AccountDBID int64 `json:"accountDBID,omitempty"`
	// Номер боя  у игрока
	BattleNum int64 `json:"battleNum,omitempty"`
}

func (fd FullData) TotalFarm() int64 {
	return fd.Credits - fd.BattleData_Cost.Total()
}
