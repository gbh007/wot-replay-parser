package parser

import (
	"fmt"
)

var BrokenDataError = fmt.Errorf("data is broken")

func ParseRaw(rawData []byte) ([]byte, []byte, error) {
	const (
		numberLen      = 4
		firstNumberPos = 8
	)

	if len(rawData) < 4 ||
		rawData[0] != '\x12' ||
		rawData[1] != '\x32' ||
		rawData[2] != '\x34' ||
		rawData[3] != '\x11' {
		return nil, nil, BrokenDataError
	}

	tp := rawData[4]

	battleInfoStartPos := firstNumberPos + numberLen
	battleInfoLen := numberFromData(rawData, firstNumberPos, numberLen)
	battleInfoRaw := rawData[battleInfoStartPos : battleInfoStartPos+battleInfoLen]

	// Нет подробных данных выходим
	if tp == 1 {
		return battleInfoRaw, []byte{}, nil
	}

	fullDataEnvelopeStartPos := firstNumberPos + numberLen + battleInfoLen + numberLen
	fullDataEnvelopeLen := numberFromData(rawData, firstNumberPos+numberLen+battleInfoLen, numberLen)
	fullDataEnvelopeRaw := rawData[fullDataEnvelopeStartPos : fullDataEnvelopeStartPos+fullDataEnvelopeLen]

	return battleInfoRaw, fullDataEnvelopeRaw, nil
}

func numberFromData(data []byte, startPos, l int) int {
	tmp := []byte{}

	for index := startPos; index < startPos+l; index++ {
		tmp = append(tmp, data[index])
	}

	sum := 0

	for index := len(tmp); index > 0; index-- {
		sum *= 256
		sum += int(tmp[index-1])
	}

	return sum
}
