package parser

import (
	"app/model"
	"encoding/json"
	"fmt"
	"log"
)

func Parse(rawData []byte) (*model.BattleInfo, *model.FullData, error) {
	battleInfoRaw, fullDataEnvelopeRaw, err := ParseRaw(rawData)
	if err != nil {
		return nil, nil, err
	}

	battleInfo := &model.BattleInfo{}

	err = json.Unmarshal(battleInfoRaw, &battleInfo)
	if err != nil {
		log.Println(len(battleInfoRaw), string(battleInfoRaw))
		return nil, nil, err
	}

	// Нет подробных данных выходим
	if len(fullDataEnvelopeRaw) == 0 {
		return battleInfo, &model.FullData{}, nil
	}

	fullDataEnvelope := []*model.FullDataEnvelope{}

	err = json.Unmarshal(fullDataEnvelopeRaw, &fullDataEnvelope)
	if err != nil {
		log.Println(len(fullDataEnvelopeRaw), string(fullDataEnvelopeRaw))
		return nil, nil, err
	}

	if len(fullDataEnvelope) < 1 {
		return nil, nil, fmt.Errorf(`len(data) < 1`)
	}

	var fullData *model.FullData

	for name := range fullDataEnvelope[0].Personal {
		if name == "avatar" {
			continue
		}

		fullData = fullDataEnvelope[0].Personal[name]
		break
	}

	if fullData == nil {
		return nil, nil, fmt.Errorf(`fullData == nil`)
	}

	return battleInfo, fullData, nil
}
